@extends('layouts.app', ['active' => 'support'])

@section('content')
<div class="row">
    <div class="col-lg-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item" aria-current="page"><a href="{{url('home')}}">Dashboard</a></li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{url('business')}}">Equipos</a></li>
            <li class="breadcrumb-item active" aria-current="page">Crear</li>
          </ol>
        </nav>
        <div class="card">
            <div class="header">
                <h4 class="title">Crear equipo</h4>
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <form class="form-horizontal" action="{{route('support.store')}}" method="post">
                            @csrf
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Numero</label>
                                    <input type="text" class="form-control" placeholder="Numero" name="number">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Tipo</label>
                                    <input type="text" class="form-control" placeholder="Tipo" name="type">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Descripción</label>
                                    <input type="text" class="form-control" placeholder="Descripción" name="description">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <label>Empresa</label>
                                    <select name="business_id" class="form-control">
                                        @foreach(Auth::user()->businesses as $business)
                                            <option value="{{$business->id}}">{{$business->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-info btn-fill pull-right">
                                    <i class="pe-7s-diskette"></i>
                                    Guardar
                                </button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
