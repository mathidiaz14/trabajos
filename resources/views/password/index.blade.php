@extends('layouts.app', ['active' => 'work'])

@section('css')
    <style>
        .table th:hover
        {
           cursor: pointer; 
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Contraseñas</li>
          </ol>
        </nav>
        <div class="card">
            <div class="header">
                <div class="row">
                    <div class="col-xs-6">
                        <h4 class="title">Contraseñas</h4>
                        <p class="category">Contraseñas registradas</p>
                    </div>
                    <div class="col-xs-6 text-right">
                    	<a href="{{url('password/create')}}" class="btn btn-success btn-fill">
                            <i class="fa fa-plus"></i>
                            Agregar
                        </a>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="table table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                              <th scope="col">Contraseña <small><i class="fa fa-sort"></i></small></th>
                              <th scope="col">Descripción <small><i class="fa fa-sort"></i></small></th>
                              <th scope="col">Empresa <small><i class="fa fa-sort"></i></small></th>
                              <th>Editar</th>
                              <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(Auth::user()->passwords as $password)
                                <tr>
                                    <td>{{$password->pass}}</td>
                                    <td>{{$password->description}}</td>
                                    <td>{{$password->business->name}}</td>
                                    <td>
                                        <a href="{{route('password.edit', $password->id)}}" class="btn btn-primary btn-fill">
                                            <i class="fa fa-edit"></i>
                                            Editar
                                        </a>
                                    </td>
                                    <td>
                                        <form action="{{ url('password', $password->id)}}" method="POST">
                                            @csrf
                                            <input type='hidden' name='_method' value='DELETE'>
                                            <button class="btn btn-danger btn-fill">
                                                <i class="fa fa-trash"></i>
                                                Eliminar
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
       $(document).ready(function(){ 
            $("#table").tablesorter(); 
        }); 
    </script>
@endsection