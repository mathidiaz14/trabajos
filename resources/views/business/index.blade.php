@extends('layouts.app', ['active' => 'business'])

@section('content')
<div class="row">
    <div class="col-lg-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Clientes</li>
          </ol>
        </nav>
        <div class="card">
            <div class="header">
                <div class="row">
                    <div class="col-xs-6">
                        <h4 class="title">Clientes</h4>
                        <p class="category">Clientes registradas</p>
                    </div>
                    <div class="col-xs-6 text-right">
                        <a href="{{url('business/create')}}" class="btn btn-success btn-fill">
                            <i class="fa fa-plus"></i>
                            Agregar
                        </a>
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="table table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                              <th scope="col">Nombre</th>
                              <th scope="col">Descripción</th>
                              <th scope="col">Dirección</th>
                              <th scope="col">Contacto</th>
                              <th scope="col">Ver</th>
                              <th scope="col">Editar</th>
                              <th scope="col">Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(Auth::user()->businesses as $business)
                                <tr>
                                    <td>{{$business->name}}</td>
                                    <td>{{$business->description}}</td>
                                    <td>{{$business->address}}</td>
                                    <td>{{$business->contact}}</td>
                                    <td>
                                        <a href="{{url('business', $business->id)}}" class="btn btn-success btn-fill">
                                            <i class="fa fa-eye"></i>
                                            Ver
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{route('business.edit', $business->id)}}" class="btn btn-primary btn-fill">
                                            <i class="fa fa-edit"></i>
                                            Editar
                                        </a>
                                    </td>
                                    <td>
                                        <form action="{{ url('business', $business->id)}}" method="POST">
                                            @csrf
                                            <input type='hidden' name='_method' value='DELETE'>
                                            <button class="btn btn-danger btn-fill">
                                                <i class="fa fa-trash"></i>
                                                Eliminar
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
