<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
	protected $dates = ['date', 'expires'];

    public function business()
    {
        return $this->belongsTo('App\Models\Business');
    }

    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }
}
