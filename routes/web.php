<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PeopleController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\NoteController;
use App\Http\Controllers\BusinessController;
use App\Http\Controllers\WorkController;
use App\Http\Controllers\SupportController;
use App\Http\Controllers\PasswordController;
use App\Http\Controllers\ConversationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/'                          , [HomeController::class, 'index'])->middleware('auth');
Route::get('/home'                      , [HomeController::class, 'index'])->middleware('auth');

Route::resource('people'                , PeopleController::class)->middleware('auth');
Route::resource('category'              , CategoryController::class)->middleware('auth');
Route::resource('note'                  , NoteController::class)->middleware('auth');
Route::resource('business'              , BusinessController::class)->middleware('auth');
Route::resource('work'                  , WorkController::class)->middleware('auth');
Route::resource('support'               , SupportController::class)->middleware('auth');
Route::resource('password'              , PasswordController::class)->middleware('auth');
Route::resource('conversation'          , ConversationController::class)->middleware('auth');
Route::get('conversation/search/{id}'   , [ConversationController::class, 'search'])->middleware('auth');

Route::post('search'                    , [HomeController::class, 'search'])->middleware('auth');

